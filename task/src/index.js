import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Table from './Table';
import FirstPg from './FirstPg';
import './index.css';
import { Route, Link, BrowserRouter as Router } from 'react-router-dom';

const routing = (
  <Router>
    <div>
      <Route path="/hp" component={App} />
      <Route path="/firstpage" component={FirstPg} />
      <Route path="/mm" component={Table} />
    </div>
  </Router>
)

ReactDOM.render(
  routing,
  document.getElementById('root')
);
