import React, { Component } from 'react';
import './App.css';

class FirstPg extends Component{
    render()
    {
        return(
            <div className="FirstPg">
                <div className="App-header">
                    <h1> Main Menu</h1>
                </div>
                    <hr />
                    <a href="#">Create/Access Job(s)</a> <br />
                    <a href="#">Create/Access Scenario(s)</a> <br />
                    <a href="#">Access Execution Results</a> <br />
                    <a href="#">Log Off</a>
            </div>
        );
    }
}

export default FirstPg;